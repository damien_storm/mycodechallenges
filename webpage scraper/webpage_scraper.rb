require 'rubygems'
require 'mechanize'

def scrape(page)
  text = page.body
  # puts text
  index_start = text.index('>PTB<')
  text = text[index_start..-1]
  index_start = text.index('Departed:  ') + 11
  index_end = text.index('* <a') - 2
  ' | ' + text[index_start..index_end]
end

agent = Mechanize.new
t = Time.now
times_late = 0
minutes_late = 0
num_of_days = 180
(1..num_of_days).each do |i|
  minutes = 0
  t -= 86_400 unless i == 0
  month = t.to_s[5, 2]
  day = t.to_s[8, 2]
  url = 'http://dixielandsoftware.net/cgi-bin/gettrain.pl?seltrain=80&selyear=2014&selmonth=' + month + '&selday=' + day
  page = agent.get(url)
  date = t.to_s[0..9]
  status = scrape(page)
  if status.include?('late')
    times_late += 1
    if status.include?('hour')
      index_start = status.index('| ') + 2
      index_end = status.index(' hour')
      minutes = status[index_start..index_end].to_i * 60
    end
    if status.include?('minute')
      status.include?('hours') ? index_start = status.index('and ') + 4 : index_start = status.index('| ') + 2
      index_end = status.index(' minute')
      minutes += status[index_start..index_end].to_i
    end
  end
  minutes_late += minutes
  puts date + status + ' | ' + minutes.to_s
end
# num_of_days += 1
average_late = minutes_late.to_f / num_of_days.to_f
puts average_late.to_f
# puts 'Minutes late: ' + minutes_late.to_s
# puts 'Number of days parsed: ' + num_of_days.to_s + ' | Average minutes late: ' + average_late.to_s
