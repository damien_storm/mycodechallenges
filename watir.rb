require 'watir-webdriver'
require 'headless'

headless = Headless.new
headless.start
browser = Watir::Browser.new :firefox
browser.goto('www.google.com')
browser.screenshot.save 'browser_loaded.png'
@headless = headless
@browser = browser
