folder = './GoogleCodeJam/Reverse Words/'
input_file = "#{folder}B-large-practice.in"
output_file = "#{folder}#{File.basename(input_file)}_out.txt"
current_input = Array.new
i = -1

# binding.pry
def find_solution(input)
  output = String.new
  input.split.reverse.each { |w| output << w + ' ' }
  output.chop
end

File.delete(output_file) if File.exist?(output_file)
new_output = File.open(output_file, "w+")
File.open(input_file, "r") do |f|
  f.each_line do |line|
    i += 1
    next if i == 0
    results = find_solution(line)
    result = "Case ##{i}: #{results}"
    puts result
    new_output.puts "#{result}"
  end
end
new_output.close
