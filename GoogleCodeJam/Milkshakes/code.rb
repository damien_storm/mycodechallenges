class Array
  def odd_values
    self.values_at(* self.each_index.select {|i| i.odd?})
  end
  def even_values
    self.values_at(* self.each_index.select {|i| i.even?})
  end
end
folder = './GoogleCodeJam/Milkshakes/'
folder = './'
input_file_location = "#{folder}B-small-practice.in"
output_file_location = "#{folder}#{File.basename(input_file_location)}_out.txt"
input_array = Array.new

# delete the prevous output file and create a new one
File.delete(output_file_location) if File.exist?(output_file_location)
new_output = File.new(output_file_location, "w+")

# read the input file into an array
# File.open(input_file_location, "r") { |fi| fi.each_line { |line| input_array << line.chomp } }
input_file = File.open(input_file_location, 'r')
input_string = input_file.read
input_array = input_string.split("\n")
input_file.close

output = Array.new
current_case = Array.new
num_customers = 0
i = 0
j = 0

# go through the array of the file to create an output array of each case
input_array[1..-1].each do |data|
  next if data.to_i == 0
  i += 1
  num_customers = data.to_i if i == 2
  current_case << data
  if i == num_customers + 2
    output << current_case.dup
    j += 1
    i = 0
    current_case.clear
  end
end

i = 0
j = 0
num_flavors = 0
num_customers = 0
impossible = false
solutions = Array.new
result = String.new
flavors = String.new
binding.pry
output.each do |c|
  flavor_list = Array.new
  num_flavors = c[0]
  num_customers = c[1]
  c[2..-1].each do |pref| 
    cust_prefs = pref.split
    num_cust_flavors = cust_prefs.shift
    cust_flavors = cust_prefs.even_values
    cust_malts = cust_prefs.odd_values
    cust_flavors.each_with_index do |flavor, k|
      cust_malts[k].to_i == 1 ? temp_flavor = flavor.to_s + 'M' : temp_flavor = flavor.to_s
      # temp_flavor.include?("M") ? impossible = flavor_list.include?(temp_flavor.gsub("M", '')) : impossible = flavor_list.include?("#{temp_flavor}M")
      flavor_list << temp_flavor #unless impossible
      # next unless impossible
      # break
    end
    # break if impossible
  end
  puts "Num flavors: #{num_flavors} | Flavor list count: #{flavor_list.uniq.count}"
  if flavor_list.uniq.count > num_flavors.to_i
    solutions[j] = "Case ##{j+1}: IMPOSSIBLE"
  else
    flavor_list.uniq.sort.each do |flavor|
      if flavors.include?(flavor.gsub('M', ''))
        solutions[j] = "Case ##{j+1}: IMPOSSIBLE" 
      else
        flavor.to_s.include?("M") ? result << "1 " : result << "0 " 
        flavors << "#{flavor} "
      end
      # puts "Flavor: #{flavor} | #{result}"
    end
    result.chomp
    solutions[j] = "Case ##{j+1}: #{result} | #{flavors}"
    result = String.new
    flavors = String.new
    # puts "Number of flavors: #{num_flavors}"
  end
  # puts "Number of customers: #{num_customers}"
  # puts "Flavor list: #{flavor_list.uniq}\n"
  j +=1
end
solutions.each do |s| 
  puts s 
  new_output.puts s
end
new_output.close

# this result needs to be accounted for: Case #38: 0 1 0 1 0 1  | 2 2M 5 5M 6 6M 