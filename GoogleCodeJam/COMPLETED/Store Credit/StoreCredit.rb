folder = './GoogleCodeJam/Store Credit/'
input_file = "#{folder}A-small-practice.in"
output_file = "#{folder}#{File.basename(input_file)}_out.txt"
current_input = Array.new
i = -1

# binding.pry
def find_solution(input)
  output = Array.new
  credit = input[0].to_i
  prices = input[2].split
  prices.each_with_index do |p1, i|
    prices.each_with_index do |p2, j|
      next if j == i
      if p1.to_i + p2.to_i == credit
      # puts "#{i}-#{p1} | #{j}-#{p2}"
      # next if output.include?("#{i+1}.#{j+1}") || output.include?("#{j+1}.#{i+1}")
        output << i+1
        output << j+1
        output.sort!
        puts "Credit: #{credit} | Solution: #{p1.to_i} + #{p2.to_i}"
        return output
      end
    end
  end
  # output.each_with_index { |o, i| puts "Case ##{i+1}: #{o.gsub(".", " ")}"}
end

File.delete(output_file) if File.exist?(output_file)
new_output = File.open(output_file, "w+")
File.open(input_file, "r") do |f|
  f.each_line do |line|
    i += 1
    next if i == 0
    current_input << line.gsub("\n", '')
    if i % 3 == 0
      results = find_solution(current_input)
      result = "Case ##{i/3}: #{results[0]} #{results[1]}"
      puts result
      new_output.puts "#{result}"
      current_input.clear
    end
  end
end
new_output.close
