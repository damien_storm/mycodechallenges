require 'benchmark'

string1_1 = "a1b2c3d4e5f1g2h3i4j5k1l2m3n4o5p1q2r3s4t5u1v2w3x4y5z"
string1_2 = "d4e5f1g2h3i4j5k1l"

string2_1 = "abcdefghijklmnopqrstuvwxyz"
string2_2 = "abcdfghijklmnopqrstuvwxyz"
string2_3 = "abcdefghjklmnopqrstuvwxyz"
string2_4 = "abcdefghijklmnopqrstuvwxyz"
string2_5 = "abcdfghiklmnopqrstuvwxyz"
string2_6 = "abcdefghijklmnopqrstuvwxyz"
string2_7 = "abcdefhijklnopqrstuvwxyz"
string2_8 = "abcdefghiklmnopqrstuvwxyz"
string2_9 = "abcdefghijklnopqstuvwxyz"
string2_10 = "abcdefghijklmnoqrstuvxyz"

string3_1 = "abcdefghijklm"
string3_2 = "nopqrstuvwxyz"

string4_1 = "how about punctuation and spaces"
string4_2 = 'h@ o !!w$@ a b! @ou $  t% ^p& u*n( c)t **u& ^ at % $i o# %n   %& an## d sp #$ac#e@  @s'

string5_1 = "ARE YOU HANDLING CASE CORRECTLY, yes?"
string5_2 = "are you handling case correctly, yes?"

string6_1 = "We the People of the United States, in Order to form a more perfect Union, establish Justice, insure domestic Tranquility, provide for the common defence, promote the general Welfare, and secure the Blessings of Liberty to ourselves and our Posterity, do ordain and establish this Constitution for the United States of America."
string6_2 = "        When in the Course of human events, it becomes necessary for one people to dissolve the political bands which have connected them with another, and to assume among the powers of the earth, the separate and equal station to which the Laws of Nature and of Nature's God entitle them, a decent respect to the opinions of mankind requires that they should declare the causes which impel them to the separation. We hold these truths to be self-evident, that all men are created equal, that they are endowed by their Creator with certain unalienable Rights, that among these are Life, Liberty and the pursuit of Happiness.--That to secure these rights, Governments are instituted among Men, deriving their just powers from the consent of the governed, --That whenever any Form of Government becomes destructive of these ends, it is the Right of the People to alter or to abolish it, and to institute new Government, laying its foundation on such principles and organizing its powers in such form, as to them shall seem most likely to effect their Safety and Happiness. Prudence, indeed, will dictate that Governments long established should not be changed for light and transient causes; and accordingly all experience hath shewn, that mankind are more disposed to suffer, while evils are sufferable, than to right themselves by abolishing the forms to which they are accustomed. But when a long train of abuses and usurpations, pursuing invariably the same Object evinces a design to reduce them under absolute Despotism, it is their right, it is their duty, to throw off such Government, and to provide new Guards for their future security.--Such has been the patient sufferance of these Colonies; and such is now the necessity which constrains them to alter their former Systems of Government. The history of the present King of Great Britain is a history of repeated injuries and usurpations, all having in direct object the establishment of an absolute Tyranny over these States. To prove this, let Facts be submitted to a candid world." 

array1 = [string1_1, string1_2]
array2 = [string2_1, string2_2, string2_3, string2_4, string2_5, string2_6, string2_7, string2_8, string2_9, string2_10]
array3 = [string3_1, string3_2]
array4 = [string4_1, string4_2]
array5 = [string5_1, string5_2]
array6 = [string6_1, string6_2]

all_arrays = [array1, array2, array3, array4, array5, array6]

def find_common_sub(array_strings)
    create_variables
    
    # trim the strings of all spaces and punctuation
    array_strings.each { |s| @a_trimmed << trim_string(s) }
    max_match_length = get_shortest

    # go through each string in the array
    @a_trimmed.each do |s1|
        
        # loop through the string from the first letter to the last    
        (0..s1.length).each do |i|
            
            # create a 2nd string starting on each letter going to the length 
            # of the shortest string of the set
            # skip combinations that have already been found or are too short
            s2 = s1.slice(i, max_match_length)
            next if s2.length < @longest || @a_subs.include?(s2)
            
            # loop through the string from the first to the last
            (0..s2.length).each do |j|
                
                # create a 3rd string starting on each letter of the 2nd string to the max match length
                # and decreasing by one character until reaching the longest length
                s3 = s2[0..s2.length - j]
                
                # check the string unless the string is too short or already found
                check_substring(s3) unless s3.length < @longest || @a_subs.include?(s3)
                
            end
        end
    end
    
    # # return an empty string if none are found
    return @return_string if @a_subs.empty?

    # remove any subs that are less than the max length
    @a_subs.delete_if { |a| a.length < @longest }
    
    # sort the array and build the return string
    @a_subs.sort!
    @a_subs.each { |a| @return_string += ";#{a}" }
    @return_string[1..-1]
end

def trim_string(s_original)
    alphabet = ('a'..'z').to_a
    alphabet.concat(('A'..'Z').to_a)
    s_new = String.new
    s_original.each_char { |c| s_new << c if alphabet.include?(c) }
    s_new
end

def get_shortest
    a_lengths = Array.new
    @a_trimmed.each { |a| a_lengths << a.length }
    a_lengths.sort[-1]
end

def check_substring(substring)
    @total+=1   
    # puts substring
    
    # check if substring is present in all of the original strings in the array
    @a_trimmed.each do |k|
        return false unless k.include?(substring)
    end
    
    # if it was add it to the substring array
    @a_subs << substring
    @longest = substring.length
    true
end

def create_variables
    @a_subs = Array.new
    @a_trimmed = Array.new
    @return_string = String.new
    @longest = 0
    @total = 0
end

@a_results = Array.new
Benchmark.bm(2) do |x|
    x.report(1) {@a_results << find_common_sub(all_arrays[0])}
    x.report(2) {@a_results << find_common_sub(all_arrays[1])}
    x.report(3) {@a_results << find_common_sub(all_arrays[2])}
    x.report(4) {@a_results << find_common_sub(all_arrays[3])}
    x.report(5) {@a_results << find_common_sub(all_arrays[4])}
    x.report(6) {@a_results << find_common_sub(all_arrays[5])}  
end

@a_results.each_with_index { |a, i| puts "#{i + 1}: [#{a}]" }