require 'pry'
require 'pry-byebug'
def find_x(map)
    coordinates = ";"
    map.each_with_index do |row, i|  
        row.each_with_index do |point, j|    
            coordinates = coordinates + "(#{i}, #{j});" if point == 'X'    
        end  
    end  
    binding.pry
    coordinates.chop!
    coordinates[1..-1]
end  

treasure_map = [["o","o","o","X","o"], ["X","o","o","o","o"], ["o","X","o","o","o"], ["o","o","o","o","X"], ["o","o","o","o","o"]]
puts find_x(treasure_map)