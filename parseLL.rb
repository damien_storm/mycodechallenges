# binding.pry
file = File.open('./LLinput.txt', 'r')
input = file.read
file.close
input.gsub!("\n", ":::")
input.gsub!("::::::", ":::")
input.gsub!(/:::\s{2,}/, "|")
input.gsub!(/\b\s\b/, "_")
input.gsub!(/\s{2,}/, "|")
input.gsub!("||", "|")
input.gsub!("|:::", ":::")
data = input.split(":::")
data.delete_if {|d| d.include?("___") or d.include?("Customer|") or d.include?("Agents_Order") }

totals = data.last.split("|")
puts "Total Customers: " + totals[0].split(" ")[1]
puts "Total Tickets: " + totals[1].split(" ")[1]
puts "Total Sales: " + totals[2].split(" ")[1]

data.slice!(-1)
denver_info = Array.new
destinations = Array.new
denver_tickets = 0
denver_total = 0
data.each do |d|
  d.gsub!("_", " ")
  current = d.split("|")
  if current[8] == "Denver"
    denver_tickets += current[1].to_f
    denver_total += current[3].gsub("$", '').to_f
    destinations << current.last
    denver_info << current.dup
    puts current[3].gsub("$", '').to_f
  end
end


destination_totals = Hash.new
destinations.uniq.sort.each { |d| destination_totals.store(d, 0) }
destinations.each { |d| destination_totals[d] += 1 if destination_totals.has_key?(d) }


puts "Denver tickets = " + denver_tickets.to_s
puts "Denver total = $" + denver_total.round(2).to_s
puts "Denver destinations: "
destination_totals.each { |k, v| puts "#{k}: #{v}" } 
puts "Denver info:"
p denver_info



# To Frankfurt: 1
# To London: 4
# To Los Angeles: 1
# To Paris: 1
# To Portland: 3
# To San Francisco: 2
# To Seattle: 1
# To Sydney: 0
# To Zurich: 2