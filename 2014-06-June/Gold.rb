def aagc(function, xrange, yrange, xres, yres)
  puts "\nx | y | y=#{function}"
  puts "---------------------------"
  rows = Array.new
  
  (0-xrange..xrange).each do |x|
    
    # evaluate the function
    new_function = function.gsub('x', "#{x.to_f}")
    y = eval(new_function)
    puts "#{x} | #{y} | #{new_function}"
    
    # build the rest of the graph 
    current_row = String.new
    

    yrange.downto(0-yrange).each do |z|
      # build the middle row if x = 0
      if x == 0
        y == z ? current_row += "X" : current_row += "-" 
        next
      end
      case z
      when y
        current_row += "X"
      when 0
        current_row += "|"
      else
        current_row += " "
      end
    end
    rows << current_row
  end
  
  # draw the graph
  rows.each {|row| puts row }
  puts 
end


aagc("x*2", 10, 20, 0.25, 0.5)