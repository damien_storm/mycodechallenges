'FizzBuzz Bronze challenge
Option Explicit


Public Function DamienStorm(nNumber)
	Dim sFizz, sBuzz, sWoof
	
	sFizz = ""
	sBuzz = ""
	sWoof = ""
	
	If nNumber mod 3 = 0 Then
		sFizz = "Fizz "
	End If

	If nNumber mod 5 = 0 Then
		sBuzz = "Buzz "
	End If

	If nNumber mod 15 = 0 Then
		sWoof = "Woof "
	End If
	
	DamienStorm = Trim(sFizz&sBuzz&sWoof)
	If DamienStorm = "" Then
		DamienStorm = nNumber
	End If

End Function

Dim sResults, sTextFileLocation
Dim objFileToWrite

sResults = DamienStorm(10)
MsgBox("["&sResults&"]")
REM sTextFileLocation = "C:\Users\damien.storm\Desktop\temp\temp.txt"
REM Set objFileToWrite = CreateObject("Scripting.FileSystemObject").OpenTextFile(sTextFileLocation,2,true)
REM objFileToWrite.WriteLine(sResults)
REM objFileToWrite.Close
REM Set objFileToWrite = Nothing
REM Execute sTextFileLocation