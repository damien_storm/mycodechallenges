'May Silver challenge
Option Explicit

Function DamienStorm(sContestInfo)
	Dim sResults
	Dim nCandyCount, nGuess, nGuesses, nGuesserCount, nLowest
	Dim sContestant, sContestants, sWinner
	Dim aContestants, aGuesses(), aJSON
	Dim i, j
	
	'Parse the JSON file
	aJSON = Split(sContestInfo, vbNewLine)
	For i = 0 to uBound(aJSON)
	
		'Get the candy count
		If InStr(aJSON(i), "candy_count") Then
			nCandyCount = TrimJSON(aJSON(i))
		End If
		
		'Get the guesser count	
		If InStr(aJSON(i), "guesser_count") Then
			nGuesserCount = TrimJSON(aJSON(i))
		End If
	
		'Get the guesser names
		If instr(aJSON(i), "name") Then
			sContestant = TrimJSON(aJSON(i))
			
			'If the name is not already in the list of contestants, add it
			If InStr(sContestants, sContestant) < 1 Then
				sContestants = sContestants&"|"&sContestant
			End if
			
		End If
		
	Next
	
	'Create an array of contestants
	sContestants = Mid(sContestants, 2)
	aContestants = Split(sContestants, "|")
	
	'Loop through the contestant array and the JSON array to get the average guess of each contestant
	ReDim aGuesses(uBound(aContestants))
	For i = 0 to uBound(aContestants)
		sContestant = aContestants(i)
		nGuess = 0
		nGuesses = 0
		For j = 0 to uBound(aJSON)
			If Instr(aJSON(j), sContestant) > 0 Then
				nGuesses = nGuesses + 1
				nGuess = nGuess + TrimJSON(aJSON(j+1))
			End If
		Next
		
		'Calculate the average guess for the contestant
		nGuess = nGuess / nGuesses
		nGuess = Abs(CInt(nCandyCount - nGuess))
		
		'Store the guess 
		aGuesses(i) = nGuess
		
	Next
	
	'Determine who was closest to the actual candy count
	nLowest = aGuesses(0)
	For i = 0 to uBound(aGuesses)
		If aGuesses(i) < nLowest Then
			nLowest = aGuesses(i)
			sWinner = aContestants(i)
		ElseIf aGuesses(i) = nLowest Then
			sWinner = sWinner &" "& aContestants(i)
		End If
	Next
	
	'Return the winner(s)
	DamienStorm = sWinner
	
End Function

Function TrimJSON(sJSONstring)
	sJSONstring = Replace(sJSONstring, "name", "")
	sJSONstring = Replace(sJSONstring, "candy_count", "")
	sJSONstring = Replace(sJSONstring, "guesser_count", "")
	sJSONstring = Replace(sJSONstring, "guess", "")
	sJSONstring = Replace(sJSONstring, ",", "")
	sJSONstring = Replace(sJSONstring, ":", "")
	sJSONstring = Replace(sJSONstring, """", "")
	sJSONstring = Replace(sJSONstring, """", "")
	sJSONstring = Replace(sJSONstring, vbTab, "")
	sJSONstring = Trim(sJSONstring)
	TrimJSON = sJSONstring
End Function