'May Gold challenge
Option Explicit
Const sTempFile = "C:\Users\damien.storm\Desktop\temp\temp.txt"
REM Const sTempFile = "K:\Orasi\Damien\temp.txt"
'================================================================
Dim sMaze, sFirstPosition, sSecondPosition
sMaze = "NES,SW,NE,ESW,NESW,EW,ESW,ESW,NESW,W;NES,NW,E,NSW,NE,ESW,NEW,NESW,NESW,ESW;NESW,W,S,N,E,NEW,SW,NES,NESW,NW;NE,W,N,ES,EW,EW,NESW,NW,NS,E;ES,W,ES,NEW,ESW,W,NS,E,NESW,W"
sFirstPosition = "5,1"
sSecondPosition = "9,19"

Function DamienStorm(sMaze, sFirstPosition, sSecondPosition)
	Dim sCurrent, sResults
	Dim aColumns, aEndPath, aMaze, aRows, aStart, aStartPath
	Dim nColumns, nRows, nStartColumn, nStartRow
	Dim i, j, k
	Dim sData
	
	'get the dimensions of the maze
	aRows = Split(sMaze, ";")
	aColumns = Split(aRows(0), ",")
	nRows = ((uBound(aRows)+1)*2)
	nColumns = ((uBound(aColumns)+1)*2)
	
	'Build an array of the maze, adding the known open spots
	ReDim aMaze(nRows, nColumns)
	For i = 1 to nRows - 1 step 2
		For j = 1 to nColumns - 1 step 2
			aColumns = Split(aRows((i-1)/2), ",")
			aMaze(i,j) = aColumns((j-1)/2)
		Next
	Next
	
	'Mark all the other spots in the maze as UNKNOWN
	For i = 0 to nRows
		For j = 0 to nColumns
			If aMaze(i,j) = "" Then
				aMaze(i,j) = "UNKNOWN"
			End If
		Next
	Next
	
	'Mark the open sections of the maze based on the maze values
	aStart = Split(sFirstPosition, ",")
	nStartRow = aStart(0)
	nStartColumn = aStart(1)
	For i = 0 to nRows
		For j = 0 to nColumns
			sCurrent = aMaze(i, j)
			If sCurrent <> "UNKNOWN" AND sCurrent <> "OPEN" Then
				For k = 1 to Len(sCurrent)
					Select Case Mid(sCurrent, k, 1)
						Case "N"
							aMaze(i-1, j) = "OPEN"
						
						Case "S"
							aMaze(i+1, j) = "OPEN"
							
						Case "E"
							aMaze(i, j+1) = "OPEN"
						
						Case "W"
							aMaze(i, j-1) = "OPEN"
					
					End Select
				Next
			End If			
		Next
	Next

	'Mark the remaining unknown sections as closed walls
	For i = 0 to nRows
		For j = 0 to nColumns
			If aMaze(i,j) = "UNKNOWN" Then
				aMaze(i,j) = "WALL"
			End If
		Next
	Next

	'Now that we have a filled out maze array, we can get all the paths from the start position
	aStartPath = PathFinder(sFirstPosition, aMaze, FALSE, FALSE)

	'Check for an intersection
	DamienStorm = PathFinder(sSecondPosition, aMaze, TRUE, aStartPath)

	
End Function

Function PathFinder(sFirstPosition, aMaze, bSearchForConnection, sSearchPath)
	Dim i, j, k
	Dim sCurrent, sPossible, sVisited
	Dim sNorth, sSouth, sEast, sWest
	Dim nColumn, nColumns, nRow, nRows
	Dim aCurrent, aPosition, aPossible, aSearch, aStart, aVisited
	Dim nCount


	aStart = Split(sFirstPosition, ",")
	sPossible = "|"&aStart(0)&","&aStart(1)&"|"
	sVisited = "|"
	nRows = uBound(aMaze, 1)
	nColumns = uBound(aMaze, 2)

	nCount = 0
	'Loop through all of the possible paths
	Do While Len(sPossible) > 0 and nCount < 100
		nCount = nCount + 1
		aPossible = Split(sPossible, "|")


		'Process all the possible cells
		For i = 0 to uBound(aPossible)
			If aPossible(i) <> "" AND InStr(sVisited, "|"&aPossible(i)) = 0 Then
				aCurrent = Split(aPossible(i), ",")
				nRow = cInt(aCurrent(0))
				nColumn = cInt(aCurrent(1))
				sCurrent  = aMaze(nRow, nColumn)

				'Check the 4 directions to see what paths are available%
				'North
				If nRow-1 >= 0 AND Instr(sVisited, "|"&nRow-1&","&nColumn) = 0 Then 
					sSouth = aMaze(nRow-1, nColumn) 
					If sSouth <> "WALL" Then
						sPossible = sPossible&nRow-1&","&nColumn&"|"
					End If
				End If
				
				'South
				If nRow+1 <= nRows AND Instr(sVisited, "|"&nRow+1&","&nColumn) = 0 Then
					sNorth = aMaze(nRow+1, nColumn) 
					If sNorth <> "WALL" Then
						sPossible = sPossible&nRow+1&","&nColumn&"|"
					End If
				End If
				
				'East
				If nColumn+1 <= nColumns AND Instr(sVisited, "|"&nRow&","&nColumn+1) = 0 Then 
					sEast = aMaze(nRow, nColumn+1) 
					If sEast <> "WALL" Then
						sPossible = sPossible&nRow&","&nColumn+1&"|"
					End If
				End If
				
				'West
				If nColumn - 1 >= 0 AND Instr(sVisited, "|"&nRow&","&nColumn-1) = 0 Then
					sWest = aMaze(nRow, nColumn-1)
					If sWest <> "WALL" Then
						sPossible = sPossible&nRow&","&nColumn-1&"|"
					End If
				End If
				
				'Add the current location to the visited list
				sVisited = sVisited&nRow&","&nColumn&"|"

				'Remove the current location from the possible list
				sPossible = Replace(sPossible, "|"&nRow&","&nColumn&"|", "|")
				
				'Check for a connection if required
				If bSearchForConnection Then
					aSearch = Split(sPossible, "|")
						For j = 0 to uBound(aSearch)
							If InStr(sSearchPath,"|"&aSearch(j)&"|") > 0 and aSearch(j) <>"" Then
								PathFinder = True
								aPosition = Split(aSearch(j), ",")
								AppendTempFile("["&aSearch(j)&"]")
								Exit Function
							End If
					Next
				End If

			End If
		Next
	
	Loop
	
	AppendTempFile("sPossible : "& sPossible & vbNewLine & "sVisited : "& sVisited)
	'Return appropriate results
	If bSearchForConnection Then
		PathFinder = False
	Else
		PathFinder = sVisited
	End If

End Function

'=================================================================



Dim sResults
Dim objFileToWrite
Dim objFileToRead

'Delete the old output file
Dim fso
Set fso = CreateObject("Scripting.FileSystemObject")
If fso.FileExists(sTempFile) Then
	fso.DeleteFile(sTempFile)
End If

'Run the funtcion
sResults = DamienStorm(sMaze, sFirstPosition, sSecondPosition)
AppendTempFile(sResults)

'Display the results
MsgBox("Result: ["&sResults&"]")

'Display the output file
Dim oShell
Set oShell = WScript.CreateObject("WSCript.shell")
oShell.run sTempFile

'=================================================================
Public Function WriteTempFile(sText)
	Set objFileToWrite = CreateObject("Scripting.FileSystemObject").OpenTextFile(sTempFile,2,true)
	objFileToWrite.WriteLine(sText) 
	objFileToWrite.Close
	Set objFileToWrite = Nothing
End Function

Public Function AppendTempFile(sText)
	Set objFileToWrite = CreateObject("Scripting.FileSystemObject").OpenTextFile(sTempFile,8,true)
	objFileToWrite.WriteLine(sText)
	objFileToWrite.Close
	Set objFileToWrite = Nothing
End Function



























