# http://rubyquiz.com/quiz1.html
def encode(original_message)
  new_message = String.new
  j = 0
  original_message.each_char do |x|
    if ('A'..'Z').include?(x.upcase)
      new_message << x.upcase 
      j += 1
      new_message << " " if j % 5 == 0
    end
  end
  new_message
end
