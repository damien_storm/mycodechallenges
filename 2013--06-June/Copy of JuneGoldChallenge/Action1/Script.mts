﻿Print "_____________________________________________________________"
Print "Test: ["&Environment("TestName")&"] started at: ["&NOW&"]"
Print "========================================"
sRow1 = "6,-23,5,11,-15,12,13,2,-19,-26,7,19,13,19,15,1,10,-12,-56,5,20"
sRow2 = "12,-13,27,-9,5,-8,-19,-5,19,-3,-7,-12,1,13"
sRow3 = "6,-23,8,4,9,10,20,2,-19,-18,19,-16,2"
sRow4 = "4,5,12,-27,19,1,20,-19,18,15,-33,-20,3,8,-26,5,6,15"
sRow5 = "5,-23,19,5,-11,9,2,-13,9"

arrRow1 = Split(sRow1, ",")
arrRow2 = Split(sRow2, ",")
arrRow3 = Split(sRow3, ",")
arrRow4 = Split(sRow4, ",")
arrRow5 = Split(sRow5, ",")


arrRows = Array(arrRow1, arrRow2, arrRow3, arrRow4, arrRow5)

dim arrCipher(4,20)
For i = 0 to Ubound(arrRow1)
	arrCipher(0,i) = CInt(arrRow1(i))
Next
For i = 0 to Ubound(arrRow2)
	arrCipher(1,i) = arrRow2(i)
Next
For i = 0 to Ubound(arrRow3)
	arrCipher(2,i) = arrRow3(i)
Next
For i = 0 to Ubound(arrRow4)
	arrCipher(3,i) = arrRow4(i)
Next
For i = 0 to Ubound(arrRow5)
	arrCipher(4,i) = arrRow5(i)
Next

nNumberOfExecutions = 10000

For i = 1 to nNumberOfExecutions
	FunctionStartTime = MercuryTimers("Timer").Start / 1000
	sSolution = GoldFunction(arrCipher)
	FunctionEndTime = MercuryTimers("Timer").Stop() / 1000
	FunctionTotalTime = FunctionEndTime - FunctionStartTime
	SumFunctionTotalTime = SumFunctionTotalTime + FunctionTotalTime
Next

AverageTime = FormatNumber(SumFunctionTotalTime/nNumberOfExecutions, 10)
TestEndTime = MercuryTimers("Timer").Stop() / 1000
TestTotalTime = TestEndTime - TestStartTime
'Window("Title:=QuickTest Print Log").Activate
Print "========================================"
Print "The result  is: ["&sSolution&"]"
Print "Average function execution time over ["&nNumberOfExecutions&"] executions: ["&AverageTime&"] seconds"
Print "Test: ["&Environment("TestName")&"] ended at : ["&NOW&"]"
Print "**************************************************************************************"
Print ""
i = 0

'Set oExcelDoc = CreateObject("Excel.Application")
'oExcelDoc.Visible = False
'oExcelDoc.DisplayAlerts = False
'oExcelDoc.Workbooks.Open sFilePath











