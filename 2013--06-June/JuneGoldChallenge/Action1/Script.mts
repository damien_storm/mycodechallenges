﻿Print "_____________________________________________________________"
Print "Test: ["&Environment("TestName")&"] started at: ["&NOW&"]"
Print "========================================"

Dim arrCipher
'original cipher:
arrCipher = Array(Array(6,-23,5,11,-15,12,13,2,-19,-26,7,19,13,19,15,1,10,-12,-56,5,20),Array(12,-13,27,-9,5,-8,-19,-5,19,-3,-7,-12,1,13),Array(6,-23,8,4,9,10,20,2,-19,-18,19,-16,2),Array(4,5,12,-27,19,1,20,-19,18,15,-33,-20,3,8,-26,5,6,15),Array(5,-23,19,5,-11,9,2,-13,9))
'original cipher with repeated words
'arrCipher = Array(Array(6,-23,5,11,-15,12,13,2,-19,-26,7,19,13,19,15,1,10,-12,-56,5,20),Array(6,-23,5,11,-15,12,13,2,-19,-26,7,19,13,19,15,1,10,-12,-56,5,20),Array(6,-23,5,11,-15,12,13,2,-19,-26,7,19,13,19,15,1,10,-12,-56,5,20),Array(12,-13,27,-9,5,-8,-19,-5,19,-3,-7,-12,1,13),Array(6,-23,8,4,9,10,20,2,-19,-18,19,-16,2),Array(4,5,12,-27,19,1,20,-19,18,15,-33,-20,3,8,-26,5,6,15),Array(5,-23,19,5,-11,9,2,-13,9))
'lew's test cipher
'arrCipher = Array(Array(2,-6,9,4,19,-1,16,-41,-2,2),Array(1,8,12,2,20,-14,13,9,-18,5,3,-9,15,6,-20,-15,25,-8,-6,-8,15,16,-18,-7,-20,-4,4),Array(2,-4,13,4,25,-5,-10,-23,-3,3),Array(6,-9,-15,20,18,-4,23,8,1,3,-9,14,-6,-20,-11,15,11,-15,19,15,-4,-20,-4,-1,-18,8,-18,-9,-16,20,4,-1,1),Array(3,20,10,13,6,-16,1,1,-20,-11,20,-7,11,-14,-13,0,0))
'lwes test cipher with only one word:is
'arrCipher = Array(Array(2,-6,9,4,19,-1,16,-41,-2,2),Array(1,8,12,2,20,-14,13,9,-18,5,3,-9,15,6,-20,-15,25,-8,-6,-8,15,16,-18,-7,-20,-4,4),Array(2,-4,13,4,25,-5,-10,-23,-3,3),Array(6,-9,-15,20,18,-4,23,8,1,3,-9,14,-6,-20,-11,15,11,-15,19,15,-4,-20,-4,-1,-18,8,-18,-9,-16,20,4,-1,1),Array(3,20,10,13,6,-16,1,1,-20,-11,20,-7,11,-14,-13,0,0),Array(1,8,12,2,20,-14,13,9,-18,5,3,-9,15,6,-20,-15,25,-8,-6,-8,15,16,-18,-7,-20,-4,4),Array(2,-6,9,4,19,-1,16,-41,-2,2))
nNumberOfExecutions = 10000

For i = 1 to nNumberOfExecutions
	FunctionStartTime = MercuryTimers("Timer").Start / 1000
	sSolution = DamienStorm(arrCipher)
	FunctionEndTime = MercuryTimers("Timer").Stop() / 1000
	FunctionTotalTime = FunctionEndTime - FunctionStartTime
	SumFunctionTotalTime = SumFunctionTotalTime + FunctionTotalTime
Next

AverageTime = FormatNumber(SumFunctionTotalTime/nNumberOfExecutions, 10)
TestEndTime = MercuryTimers("Timer").Stop() / 1000
TestTotalTime = TestEndTime - TestStartTime
Print "========================================"
Print "The result  is: ["&sSolution&"]"
Print "Average function execution time over ["&nNumberOfExecutions&"] executions: ["&AverageTime&"] seconds"
Print "Test: ["&Environment("TestName")&"] ended at : ["&NOW&"]"
Print "**************************************************************************************"
Print ""
i = 0
Window("title:=QuickTest Print Log").Activate
















