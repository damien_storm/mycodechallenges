# output dates for Commission Splits regression runs for one year from the current date
(0..365).each do |i|                                                      
  t = Time.new + (60*60*24*i)                                 
  puts (t - (60*60*24*6)).strftime("%A, %B %d") if t.sunday? && t.day < 8
end  
