﻿Option Explicit

Dim nNumberOfExecutions, x, FunctionStartTime, sResults, nTheNumber, FunctionEndTime, FunctionTotalTime, nNumberOfIterations, TestEndTime, TestStartTime, TestTotalTime
TestStartTime = MercuryTimers("TestTimer").Start / 1000

Print "_____________________________________________________________"
Print "Test: ["&Environment("TestName")&"] started at: ["&NOW&"]"
Print "========================================"

nNumberOfIterations = 0

For x = 200000 to 1000000 step 5000

	nTheNumber = x

	FunctionStartTime = MercuryTimers("FunctionTimer").Start / 1000

	sResults = DamienStorm(nTheNumber)

	FunctionEndTime = MercuryTimers("FunctionTimer").Stop() / 1000
	FunctionTotalTime = FunctionEndTime - FunctionStartTime
	If FunctionTotalTime > 300 Then
		Exit For
	End If
'	Print "Function duration for ["&nTheNumber&"]	["&FunctionTotalTime&"]	["&SecondsToTime(FunctionTotalTime)&"]"
	Print nTheNumber&"	"&FunctionTotalTime&"	"&SecondsToTime(FunctionTotalTime)
	nNumberOfIterations = nNumberOfIterations + 1
Next

TestEndTime = MercuryTimers("TestTimer").Stop() / 1000
TestTotalTime = TestEndTime - TestStartTime
TestTotalTime = SecondsToTime(TestTotalTime)

Print "========================================"
Print "The last number was: ["&nTheNumber&"] and the result  is:"
Print "["&sResults&"]"
Print "Test: ["&Environment("TestName")&"] ended at : ["&NOW&"]"
Print "Number of iterations: ["&nNumberOfIterations&"]"
Print "Total time taken was: ["&TestTotalTime&"]."
Print "**************************************************************************************"
Print ""

Function SecondsToTime(ByVal intSeconds)
	Dim hours, minutes, seconds
	
	' calculates whole hours (like a div operator)
	hours = intSeconds \ 3600
	If hours < 10 Then
		hours = "0"&hours
	End If
	
	' calculates the remaining number of seconds
	seconds = intSeconds Mod 3600
	
	' calculates the whole number of minutes in the remaining number of seconds
	minutes = seconds \ 60
	If minutes < 10 Then
		minutes = "0"&minutes
	End If

	' calculates the remaining number of seconds after taking the number of minutes
	seconds = intSeconds - (minutes * 60) - (hours * 3600)
	If seconds < 10 Then
		seconds = "0"&seconds
	End If
	' returns as a string
	SecondsToTime = hours & ":" & minutes & ":" & seconds
End Function






















































