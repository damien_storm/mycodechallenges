'**********************************************************************
'Script Name:		Damien Storm
'Author:			Damien Storm
'Created Date:		2013-07-03
'Purpose:			Submission for the July Code Challenge - Silver League
'Prerequisites:		none
'Change History:
'**********************************************************************

Option Explicit

Public Function DamienStorm(nTheNumber)
'	**********************************************************************
'	Variable Declaration
'	**********************************************************************
	Dim bPrime
	Dim i
	Dim j
	Dim nResult
	Dim sResults	

'	**********************************************************************
'	Variable Assignment
'	**********************************************************************

'	**********************************************************************
'	Business Process
'	**********************************************************************
	If nTheNumber < 2 Then
	
		'return an error message
		sResults = "You entered ["&nTheNumber&"], but no primes exist below the number 2"
	
	ElseIf nTheNumber = 2 Then
	
		'return just 2
		sResults = 2
	
	Else
	
		'add 2 to the results
		sResults = 2
		
		'Go through the rest of the numbers up to nTheNumber, but skip all even numbers after 2 because they will never be prime.
		For i = 3 to nTheNumber step 2
			bPrime = False
			
			'If the nTheNumber is 1, exit the For because its not a prime
			If i = 1 Then
				Exit For
			End If
			
			'Loop through the numbers again using the modulus function
			'Still skipping even numbers because no prime greater than 2 will never be divisible by an even number
			For j = 3 to nTheNumber step 2
				nResult = i Mod j

				'If the number is evenly divisible and the divisor is not equal to the number then exit the For because it is not a prime
				If nResult = 0 and (j <> i) Then
					bPrime = False
					Exit For
				
				'We only care about divisors less than 1/3 of the dividend because if the divisor is greater than 1/3 of the dividend we know it is a prime and need to exit the For and add the number to the list of primes
				ElseIf j >= (i / 3) Then
					bPrime = True
					Exit For
				End If
			Next
		
			'If the number was a prime, add it to the results list
			If bPrime Then
				sResults = sResults&";"&i
			End If
			
		Next
		
	End If

	'Return the results
	DamienStorm = sResults

End Function

'**********************************************************************
'End File
'**********************************************************************
Dim nNumberOfExecutions, x, FunctionStartTime, sResults, nTheNumber, FunctionEndTime, FunctionTotalTime, nNumberOfIterations, TestEndTime, TestStartTime, TestTotalTime, message, objFileToWrite

Set objFileToWrite = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Users\damien.storm\Google Drive\Orasi\Monthly Coding Challenge\July\results log_3.txt",8,true)

TestStartTime = Timer()
objFileToWrite.WriteLine("Starting at: "&NOW)

REM For x = 200000 to 1000000 step 5000
nTheNumber = 10000
sResults = DamienStorm(nTheNumber)

objFileToWrite.WriteLine("______")
objFileToWrite.WriteLine(message)

TestEndTime = Timer()
TestTotalTime = FormatNumber(TestEndTime-TestStartTime, 4)

objFileToWrite.WriteLine "========================================"
objFileToWrite.WriteLine "The last number was: ["&nTheNumber&"] and the result  is:"
REM objFileToWrite.WriteLine "["&sResults&"]"
objFileToWrite.WriteLine "Test ended at: ["&NOW&"]"
objFileToWrite.WriteLine "Number of iterations: ["&nNumberOfIterations&"]"
objFileToWrite.WriteLine "Total time taken was: ["&SecondsToTime(TestTotalTime)&"] ["&TestTotalTime&"]."
objFileToWrite.WriteLine "**************************************************************************************"
objFileToWrite.WriteLine ""


objFileToWrite.Close
Set objFileToWrite = Nothing


Function SecondsToTime(ByVal intSeconds)
	Dim hours, minutes, seconds
	
	' calculates whole hours (like a div operator)
	hours = intSeconds \ 3600
	If hours < 10 Then
		hours = "0"&hours
	End If
	
	' calculates the remaining number of seconds
	seconds = intSeconds Mod 3600
	
	' calculates the whole number of minutes in the remaining number of seconds
	minutes = seconds \ 60
	If minutes < 10 Then
		minutes = "0"&minutes
	End If

	' calculates the remaining number of seconds after taking the number of minutes
	seconds = intSeconds - (minutes * 60) - (hours * 3600)
	If seconds < 10 Then
		seconds = "0"&seconds
	End If
	' returns as a string
	SecondsToTime = hours & ":" & minutes & ":" & seconds
End Function



