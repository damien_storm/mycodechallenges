#http://eewang.github.io/blog/2013/02/20/how-to-visualize-data-with-google-charts/

require 'gchart'

data_array = [1, 4, 3, 5, 9]

bar_chart = Gchart.new(
            :type => 'bar',
            :size => '400x400',
            :bar_colors => "000000",
            :title => "My Title",
            :bg => 'EFEFEF',
            :legend => 'first data set label',
            :data => data_array,
            :filename => 'images/bar_chart.png',
            )
p bar_chart

bar_chart.file