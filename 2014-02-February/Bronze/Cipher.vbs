Option Explicit

REM Challenge Description 
REM In cryptography a cipher is a method of writing in which the content of the message is obscured and are 
REM used to protect sensitive information. One of the simplest and oldest ciphers is known as the Shift 
REM Cipher. The Shift Cipher obscures the content of the message by “shifting” every letter in the message a 
REM certain number of positions down the alphabet. With a Shift Cipher of 1 “A” would become “B”, “B” 
REM would become “C”, “R” would become “S” and “Z” would become “A”. The Bronze League Orasi Code 
REM Challenge for February 2014 is to encode a message with a Shift Cipher of 1. 
REM Shift Cipher: 
 REM A Shift Cypher iterates through each letter in a message and shifts it ahead in the alphabet. This is an 
REM example of a simple message before and after a Shift Cipher: 
REM Original Message 
 REM “This is a super secret message” 
REM Shifted Message 
 REM “Uijt jt b tvqfs tfdsfu nfttbhf” 
REM Submission Requirements: 
REM Your function should accept a single input, the message, and return a single output, the shifted 
REM message. The function name should be your name in the format FirstnameLastname. No variations on 
REM this function naming convention will be accepted. The function must comply with Orasi Coding 
REM Standards. You will be allowed 1 minute of execution time to accomplish this task. Anyone who 
REM successfully returns the correct response will be considered to have completed the challenge. A winner 
REM will be announced based on conformity to coding standards, efficiency, and creativity. 
REM Example Format: 
REM FirstnameLastname(“abc”) would return the string “bcd” 


REM Public Sub Test()
	REM msgbox("This is a test.")
REM End Sub

REM Public Function DamienStormOLD(sMessage)

	REM '========text object stuff
	REM Dim oTextFile
	REM Dim ofso
	REM Set ofso = Nothing
	REM Set oTextFile = Nothing
	REM sTextFileLocation = "C:\Users\damien.storm\Desktop\PalindromeLog2.txt"
	REM Set ofso = WScript.CreateObject("Scripting.Filesystemobject")
	REM Set oTextFile = ofso.OpenTextFile(sTextFileLocation, 8, True, -2)
	REM oTextFile.WriteLine("")
	REM oTextFile.WriteLine("Starting run at ["&Now&"].")
	REM '========text object stuff

	REM oTextFile.WriteLine(sMessage)
	REM MsgBox(sMessage)
	REM DamienStorm = nNext
REM End Function

'**********************************************************************
'Script Name:		DamienStorm
'Author:					Damien Storm
'Created Date:		2014-02-27
'Purpose:				Accepts one input, an integer, and outputs a single value, the next lowest palindromic integer. Designed for the February Coding Challenge - Bronze level.
'**********************************************************************
Public Function DamienStorm(sMessage)
'**********************************************************************
'Variable Declaration
'**********************************************************************
Dim aAlphabet
Dim aMessage
Dim bFound
Dim sCharacter
Dim sCipher
Dim i
Dim j

'**********************************************************************
'Variable Assignment
'**********************************************************************
aAlphabet = Split("a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,a", ",")
ReDim aMessage(Len(sMessage)-1)

'**********************************************************************
'Business process
'**********************************************************************
'Create an array of the individual characters in the message
For i = 0 to uBound(aMessage)
	aMessage(i) = Mid(sMessage, i+1, 1)
Next

'Go through each letter in the original message
For i = 0 to uBound(aMessage)
	bFound = False
	sCharacter = LCase(aMessage(i))
	
	'Check to see if the character is in the alphabet
	For j = 0 to uBound(aAlphabet)
		
		'If it is, add the next letter in the alphabet to the ciphered message
		If sCharacter = aAlphabet(j) Then
			sCipher = sCipher&aAlphabet(j+1)
			bFound = True
			Exit For
		End If
	Next
	
	'If the characters was not in the alphabet, add it to the ciphered message without shifting it
	If NOT bFound Then
		sCipher = sCipher&sCharacter
	End If
Next
	
'**********************************************************************
'Output Assignment
'**********************************************************************
DamienStorm = sCipher

'**********************************************************************
'End File
'**********************************************************************
End Function
Dim sMessage
sMessage = "zzz yyy xxx www | aaa bbb ccc ddd"
MsgBox("The ciphered version of ["&sMessage&"] is: ["&DamienStorm(sMessage)&"].")