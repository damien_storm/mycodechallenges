REM Challenge Description 
REM A palindrome is a sequence of characters that is the same if read forwards or backwards. Although palindromes are most frequently talked about in the context of words and phrases, the idea can also be extended to numbers. Some examples of such numbers are “123321”, “2002”, and “1”. The Silver League Orasi Code Challenge for February 2014 is to find the lowest palindromic integer above a given input. 
 
REM Submission Requirements: 
REM Your function should accept one input, an integer, and should output a single value, the next lowest palindromic integer. The function name should be your name in the format FirstnameLastname. No variations of this function naming convention will be accepted. The function must comply with Orasi Coding Standards. You will be allowed 1 minute of execution time to accomplish this task. Anyone who successfully returns the correct response will be considered to have completed the challenge. A winner will be announced based on conformity to coding standards, efficiency, and creativity.

Public Sub Test()
	msgbox("This is a test.")
End Sub

Public Function DamienStormOLD(nNumber)

	'========text object stuff
	Dim oTextFile
	Dim ofso
	Set ofso = Nothing
	Set oTextFile = Nothing
	sTextFileLocation = "C:\Users\damien.storm\Desktop\PalindromeLog2.txt"
	Set ofso = WScript.CreateObject("Scripting.Filesystemobject")
	Set oTextFile = ofso.OpenTextFile(sTextFileLocation, 8, True, -2)
	oTextFile.WriteLine("")
	oTextFile.WriteLine("Starting run at ["&Now&"].")
	'========text object stuff
	
	bFound = False
	REM nHalfLen = FormatNumber(Len(nNumber) / 2, 0)
	nHalfLen = cInt(Len(nNumber) / 2)
	nNext = nNumber
	REM oTextFile.WriteLine(nHalfLen)
	REM For i = nNumber+1 to nNumber+nNumber
	Do Until bFound
		REM nLeft = Left(i, nHalfLen)
		REM nRight = StrReverse(Right(i, nHalfLen))
		REM oTextFile.WriteLine(i&": "&nLeft&" | "&nRight)
		nNext = nNext + 1
		If Left(nNext, nHalfLen) = StrReverse(Right(nNext, nHalfLen)) Then
			REM nPalindrome = nNext
			bFound = True
			Exit Do
		End If
	Loop
	REM Next		

	If bFound Then
		sMessage = "The next palindromic number after ["&nNumber&"] is: ["&nNext&"]."
	Else
		sMessage = "No palindromic number was found between ["&nNumber&"] and ["&nNext&"]"
	End If
	oTextFile.WriteLine(sMessage)
	MsgBox(sMessage)
	DamienStorm = nNext
End Function

Public Function DamienStorm(nNumber)
'**********************************************************************
'Script Name:		DamienStorm
'Author:					Damien Storm
'Created Date:		2014-02-27
'Purpose:				Accepts one input, an integer, and outputs a single value, the next lowest palindromic integer.
'**********************************************************************
'**********************************************************************
'Variable Declaration
'**********************************************************************
Dim nHalfLength

'**********************************************************************
'Variable Assignment
'**********************************************************************
'determine how long each half of the number would be
nHalfLength = cInt(Len(nNumber) / 2)
'iterate the number to make sure the result is higher than the original number
nNumber = nNumber + 1

'**********************************************************************
'Business process
'**********************************************************************
'loop until a symetrical number is found, iterating each time
Do Until Left(nNumber, nHalfLength) = StrReverse(Right(nNumber, nHalfLength))
	nNumber = nNumber + 1
Loop

'**********************************************************************
'Output Assignment
'**********************************************************************
DamienStorm = nNumber

'**********************************************************************
'End File
'**********************************************************************
End Function

nNumber = 462264
MsgBox("The next palindromic number after ["&nNumber&"] is: ["&DamienStorm(nNumber)&"].")