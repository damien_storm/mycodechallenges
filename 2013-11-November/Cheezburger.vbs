Option Explicit

Public Function Log(sMessage)
	Dim oTextFile, ofso
	Dim sTextFileLocation
	sTextFileLocation = "C:\Users\damien.storm\Documents\SVN\Monthly Coding Challenge\November\Cheezburger_Log.txt"
	Set ofso = WScript.CreateObject("Scripting.Filesystemobject")
	Set oTextFile = ofso.OpenTextFile(sTextFileLocation, 8, True, -2)
	oTextFile.WriteLine(sMessage)
	Set ofso = Nothing
	Set oTextFile = Nothing
End Function

Public Function ClearLog()
	Dim oTextFile, ofso
	Dim sTextFileLocation
	sTextFileLocation = "C:\Users\damien.storm\Documents\SVN\Monthly Coding Challenge\November\Cheezburger_Log.txt"
	Set ofso = WScript.CreateObject("Scripting.Filesystemobject")
	Set oTextFile = ofso.OpenTextFile(sTextFileLocation, 2, True, -2)
	oTextFile.WriteLine("Clearing log")
	Set ofso = Nothing
	Set oTextFile = Nothing
End Function

class Kitten

	Public x_location
	Public y_location
	Private goal
	Private location_history
	Private mazeArray

	Private Sub Class_Initialize   ' Setup Initialize event.
		x_location= 9
		y_location= 9
		goal="3,4"
		mazeArray = Array("ES","WS","","ES","EW","EW","EW","EW","EW","WS","NS","NS","","NE","EW","EW","EW","EW","W","NS","NS","NS","","ES","EW","EW","EW","EWS","EW","NWS","NS","NE","X","NS","","","","NS","","NS","NS","S","S","NS","ES","EW","WS","NS","","NS","NS","NS","NES","NWS","NS","S","NS","NS","ES","NW","NS","NS","NS","NS","NS","NS","NS","NS","NS","S","NS","NS","NS","NS","NS","NS","N","NS","NS","NS","NS","NE","NW","NS","NS","NE","EW","NW","NS","NS","NE","EW","EW","NW","NE","EW","EW","EW","NEW","NW")
	End Sub

	Private Function add_to_location_history()
		location_history = location_history & ";" & x_location & "," & y_location
	End Function
	
	Public Function get_directions(index)
		get_directions = mazeArray(index)
	End Function

	Public Function Where()
		Where = x_location & "," & y_location
	End Function

	Public Function Move(direction)
		Select Case lcase(direction)
			Case "north"
				If inStr(Look(), "N") Then
					add_to_location_history()
					REM moveKitten("north")
					y_location = y_location -1
				End If
			Case "south"
				If inStr(Look(), "S") Then
					add_to_location_history()
					REM moveKitten("south")
					y_location = y_location +1
				End If
			Case "east"
				If inStr(Look(), "E") Then
					add_to_location_history()
					REM moveKitten("east")
					x_location = x_location +1
				End If
			Case "west"
				If inStr(Look(), "W") Then
					add_to_location_history()
					REM moveKitten("west")
					x_location = x_location -1
				End If
		End Select
	End Function

	Public Function Look()
		Look = MazeArray(x_location + (y_location * 10))
	End Function

	Public Function Remember()
		Remember = location_history
	End Function

	Public Function Smell()
		Smell = goal
	End Function

End Class

Public Function TestWrapper()

	Dim oKitten
	Dim sNextMove, sGoal
	Dim nExit, nMoves, nMazeCounter
	Dim arrMazeInfo()
	Dim i

	Set oKitten = New Kitten
	
	nMoves = 0
	nExit = 5000
	sGoal = oKitten.Smell
	nMazeCounter = 0
	

	Do 'While nMoves < nExit

		'store the possible movement information about this square of the maze
		If InStr(1, oKitten.Remember, oKitten.Where, 1) = 0 Then
			nMazeCounter = nMazeCounter + 1
			Redim Preserve arrMazeInfo(nMazeCounter)
			arrMazeInfo(nMazeCounter) = oKitten.Where&"-"&oKitten.Look
		End If	
		
		REM If nMazeCounter = 100 Then
			REM Msgbox("Reached 100 maze squares")
		If oKitten.Where = oKitten.Smell Then
		
			For i = 0 to uBound(arrMazeInfo)
				Log(arrMazeInfo(i))
			Next
			MsgBox("Found the cheezburger at ["&oKitten.Where&"] in  ["&nMoves&"]")
			Exit Do
		End If
		
		'Get the next move from the function
		sNextMove = DamienStorm(oKitten)
	
		'Move in the selected direction
		oKitten.Move(sNextMove)
		
		nMoves = nMoves + 1
		REM Log("Move: ["&nMoves&"]")
		REM Log(oKitten.Where&" | "&sGoal)
		REM If oKitten.Where = sGoal Then
			REM MsgBox("Found the cheezburger in ["&nMoves&"].")
			REM Exit Do
		REM End If
		
		REM If nMoves = nExit Then
			REM MsgBox("Reached ["&nExit&"] moves, exiting loop")
			REM Exit Do
		REM End If
	Loop 	

End Function

REM Kitten.Where – Returns the current location of the kitten. This will be formatted as “x,y” 

REM Kitten.Look – Returns the possible directions the kitten can travel from its current location. This will be formatted as a string of uppercase letters. If the kitten could move North, South, and East this string would be “NES” 

REM Kitten.Remember – Returns a list of every location the kitten has been at. This list will be semi-colon delimited and formatted “x,y;x2,y2;x3,y3” 

REM Kitten.Smell - Returns the location of the cheeseburger. This will be formatted as “x,y”	

REM Where: [9,9] - Look: [NW] - Remember: [], Smell: [3,4]

Public Function DamienStorm(oKitten)
	Dim aPossible
	Dim nMazeCounter, nStringLen
	Dim sDirection, sKittenLocation, sPossible
	Dim i

	'Get the current location of the Kitten
	sKittenLocation = oKitten.Where
	
	'Create an array of possible movement directions
	sPossible = oKitten.Look
	nStringLen = Len(sPossible)
	ReDim aPossible(nStringLen - 1)
	For i = 0 To nStringLen - 1
		aPossible(i) = Mid(sPossible, i + 1, 1)
	Next		
	
	'get a random direction to move in
	sDirection = Random(0, UBound(aPossible))
	sDirection = aPossible(sDirection)
	
	'convert the result to a movement command
	Select Case sDirection
		Case "N"
			sDirection = "north"
			
		Case "S"
			sDirection = "south"
			
		Case "E"
			sDirection = "east"
		
		Case "W"
			sDirection = "west"
		
	End Select

	
	
	DamienStorm = sDirection
End Function


Function  Random(LowerLimit, UpperLimit)
	Randomize
	Random = Int((UpperLimit - LowerLimit + 1) * Rnd +  LowerLimit)
End Function

REM Dim i
REM Dim sTimes, sMoves, sTemp
REM Dim arrTimes, arrMoves, arrTemp
REM Dim nRuns, nAvgTime, nAvgMoves, nTotalTime, nTotalMoves
REM nRuns = 500
REM For i = 1 to nRuns
	REM sTemp = DamienStorm()
	REM arrTemp = Split(sTemp, "|")
	REM sMoves = sMoves&";"&arrTemp(0)
	REM sTimes = sTimes&";"&arrTemp(1)
	REM Erase arrTemp
REM Next

REM sMoves = Right(sMoves, Len(sMoves) - 1)
REM sTimes = Right(sTimes, Len(sTimes) - 1)
REM arrMoves = split(sMoves, ";")
REM arrTimes = split(sTimes, ";")

REM nTotalTime = 0
REM nTotalMoves = 0

REM For i = 0 to uBound(arrMoves)
	REM nTotalTime = nTotalTime + arrTimes(i)
	REM nTotalMoves = nTotalMoves + arrMoves(i)
REM Next

REM nAvgTime = FormatNumber(nTotalTime / uBound(arrTimes), 5)
REM nAvgMoves = FormatNumber(nTotalMoves / uBound(arrMoves), 0)

REM oTextFile.Writeline "Number of iterations = ["&nRuns&"]"&Chr(13)&"Average time = ["&nAvgTime&"] seconds"&Chr(13)&"Average moves = ["&nAvgMoves&"] moves"

ClearLog()
TestWrapper()

REM Msgbox("Testing complete.")




